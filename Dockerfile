FROM python:3.8

ARG PROJNAME=pswrds-app
ARG POETRY_VERSION=1.1.6

ENV PROJNAME=${PROJNAME}
ENV POETRY_VERSION=${POETRY_VERSION}

RUN pip install "poetry==$POETRY_VERSION"

WORKDIR /${PROJNAME}

COPY ./poetry.lock /${PROJNAME}/poetry.lock
COPY ./pyproject.toml /${PROJNAME}/pyproject.toml

COPY ./src /${PROJNAME}/src

RUN poetry install --no-dev --no-interaction --no-ansi
