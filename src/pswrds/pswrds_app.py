from flask import Flask, render_template, request
from .model import BertModel

MODEL_CHECKPOINT = '/home/xfs/dmia-passwords/model.pth'

app = Flask(__name__)
model = BertModel()
model.load_from_checkpoint(MODEL_CHECKPOINT)


@app.route('/', methods=['post', 'get'])
def calculate_password_frequency():
    message = ''

    if request.method == 'POST':
        password = request.form.get('password')
        if password:
            prediction = round(model.predict(password), 2)
            message = f'{password} occurs {prediction} times in a million random passwords.'
        else:
            message = 'Please enter password.'

    return render_template('pswrds.html', message=message)
