import argparse

import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F

from pathlib import Path

from transformers import BertTokenizer
from transformers import BertForSequenceClassification
from transformers import AdamW

from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

DEFAULT_DATA_PATH = 'data/train.csv/train.csv'
HPARAMS = {
    'batch_size': 128,
    'num_workers': 8,
    'valid_size': 0.01
}

tokenizer = BertTokenizer.from_pretrained('bert-base-cased')


class PasswordsDataset(Dataset):
    def __init__(self, passwords, times=None):
        self.passwords = passwords
        self.times = times

    def __len__(self):
        return len(self.passwords)

    def __getitem__(self, idx):
        password = self.passwords[idx]
        sample = {
            'password': password
        }
        if self.times is not None:
            sample['times'] = self.times[idx]
        return sample


def collate_fn(samples):
    passwords = [sample['password'] for sample in samples]
    batch = tokenizer(passwords, return_tensors='pt', padding=True, truncation=True, is_split_into_words=False)

    if is_train := 'times' in samples[0]:
        batch['times'] = torch.from_numpy(np.array([sample['times'] for sample in samples], dtype=np.float32))

    return batch


class BertFineTuner(pl.LightningModule):
    def __init__(self, data_path, valid_size, batch_size, num_workers):
        super().__init__()
        self.save_hyperparameters()

        self.model = BertForSequenceClassification.from_pretrained('bert-base-cased')
        self.model.classifier = nn.Linear(768, 1)

    def forward(self, input_ids, attention_mask=None):
        return self.model(
            input_ids,
            attention_mask=attention_mask,
        ).logits.squeeze(1)

    def training_step(self, batch, batch_idx):
        times_gt = batch['times']

        times_pred = self(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
        )

        loss = torch.sqrt(F.mse_loss(times_pred, torch.log(1 + times_gt)))

        self.log('train_loss', loss, on_step=False, on_epoch=True, prog_bar=True, logger=True, sync_dist=True)

        return loss

    def validation_step(self, batch, batch_idx):
        times_gt = batch['times']

        times_pred = self(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
        )

        loss = torch.sqrt(F.mse_loss(times_pred, torch.log(1 + times_gt)))

        self.log('val_loss', loss, prog_bar=True, logger=True, sync_dist=True)
        return loss

    def configure_optimizers(self):
        "Prepare optimizer and schedule (linear warmup and decay)"

        model = self.model
        no_decay = ['bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': 0.001},
            {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]
        optimizer = AdamW(optimizer_grouped_parameters, lr=5e-5)

        return optimizer

    def setup(self, stage=None):
        trainval_df = pd.read_csv(self.hparams.data_path)
        trainval_df.Password = trainval_df.Password.astype(str)

        if stage == 'fit' or stage is None:
            if self.hparams.valid_size:
                train_df, valid_df = train_test_split(
                    trainval_df,
                    test_size=self.hparams.valid_size,
                    random_state=42
                )
                self.train_dataset = PasswordsDataset(
                    train_df.Password.values,
                    times=train_df.Times.values
                )
                self.valid_dataset = PasswordsDataset(
                    valid_df.Password.values,
                    times=valid_df.Times.values
                )
            else:
                self.train_dataset = PasswordsDataset(
                    trainval_df.Password.values,
                    times=trainval_df.Times.values
                )

        if stage == 'test' or stage is None:
            raise NotImplementedError

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            shuffle=True,
            batch_size=self.hparams.batch_size,
            num_workers=self.hparams.num_workers,
            drop_last=True,
            collate_fn=collate_fn
        )

    def val_dataloader(self):
        return DataLoader(
            self.valid_dataset,
            batch_size=self.hparams.batch_size,
            num_workers=self.hparams.num_workers,
            collate_fn=collate_fn
        ) if hasattr(self, 'valid_dataset') else None

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.hparams.batch_size,
            num_workers=self.hparams.num_workers,
            collate_fn=collate_fn
        )


class BertModel:
    def train(self, hparams):
        wrapped_model = BertFineTuner(hparams.data_path, hparams.valid_size, hparams.batch_size, hparams.num_workers)

        trainer = pl.Trainer(
            gpus=hparams.gpus,
            accelerator=hparams.accelerator,
            deterministic=True,
            max_steps=hparams.max_steps,
            accumulate_grad_batches=hparams.accumulate_grad_batches
        )
        trainer.fit(wrapped_model)
        torch.save(wrapped_model.model.state_dict(), args.ckpt_path)
        self.model = wrapped_model.model
        self.model.eval()

    def load_from_checkpoint(self, model_path):
        self.model = BertForSequenceClassification.from_pretrained('bert-base-cased')
        self.model.classifier = nn.Linear(768, 1)
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()

    def predict(self, password):
        with torch.no_grad():
            batch = tokenizer([password], return_tensors='pt', padding=True, truncation=True, is_split_into_words=False)
            times_pred = self.model(
                batch["input_ids"],
                attention_mask=batch["attention_mask"],
            ).logits.squeeze()

            times_pred = (torch.exp(times_pred) - 1).squeeze().item()
        return times_pred


def get_args(args=None):
    parser = argparse.ArgumentParser(
        description='Train segmentation model.'
    )
    parser.add_argument(
        '-d', '--data-path',
        type=Path, required=True,
        help='Path to train data.'
    )
    parser.add_argument(
        '-p', '--ckpt-path',
        type=Path, required=True,
        help='Path where to save checkpoint.'
    )
    parser.add_argument(
        '-v', '--valid-size',
        type=int, default=0.1,
        help='Size of validation split'
    )
    parser.add_argument(
        '-b', '--batch-size',
        type=int, default=128,
        help='Batch size.'
    )
    parser.add_argument(
        '-s', '--max-steps',
        type=int, default=1000,
        help='Steps to train.'
    )
    parser.add_argument(
        '-c', '--accumulate-grad-batches',
        type=int, default=4,
        help='Epochs to train.'
    )
    parser.add_argument(
        '-w', '--num-workers',
        type=int, default=0,
        help=(
            'How many subprocesses to use for data loading. '
            '0 means that the data will be loaded in the main process.'
        )
    )
    parser.add_argument(
        '-g', '--gpus',
        nargs='+', type=int, default=None,
        help='Which GPUs to use. If no GPUs specified, then CPU is used.'
    )
    parser.add_argument(
        '-a', '--accelerator',
        type=str, default=None,
        help='Which distributed backend to use (dp, ddp, ddp2, ddp_spawn).'
    )
    return parser.parse_args(args)


if __name__ == '__main__':
    args = get_args()
    model = BertModel()
    model.train(args)
